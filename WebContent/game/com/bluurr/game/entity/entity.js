var Entity = Class.create(
{
	initialize:function() 
	{
		this.sprite = null;
		this.locationX = 0;
		this.locationY = 0;
	},
	/**
	 * 
	 * @returns {___sprite0}
	 */
	getSprite:function()
	{
		return this.sprite;
	},
	/**
	 * Loads the main sprite image for this entity.
	 * @param textureTarget
	 * @param anchorX
	 * @param anchorY
	 */
	setSprite:function(textureTarget, anchorX, anchorY)
	{
		this.sprite = new PIXI.Sprite(PIXI.Texture.fromImage(textureTarget));
		this.sprite.anchor.x = anchorX;
		this.sprite.anchor.y = anchorY;
	},
	/**
	 * Sets the position of the person, this will most likly be moved to a entity class
	 * @param locationX
	 * @param locationY
	 */
	setPosition:function(locationX, locationY)
	{
		this.locationX = locationX;
		this.locationY = locationY;
	},
	/**
	 * Render the entity to the game screen
	 */
	preRender:function(stage)
	{
		this.sprite.position.x = this.locationX;
		this.sprite.position.y = this.locationY;
	}
});





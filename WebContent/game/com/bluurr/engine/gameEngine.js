var GameEngine = Class.create(
{
	initialize:function(configData) 
	{
		this.configContainer = configData.evalJSON();
	},
	getConfigs:function()
	{
		return this.configContainer;
	},
	getSprite:function(name)
	{
		return jQuery.grep(this.configContainer.sprites, function(e){ return e.name == name; });
	}
});